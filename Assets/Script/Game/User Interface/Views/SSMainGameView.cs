﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SSMainGameView : RDView
{
	#region Fields and Properties

	public Image ShiftBarBackgroundImage {get; set;}
	public Image ShiftBarFill {get; set;}
	public Image ShiftBarRefill {get; set;}
	public Text SheepStatusText {get; set;}
	public Text ShiftStatusText {get; set;}

	public Text TimeRemainingText {get; set;}
	public Text SheepEatenText {get; set;}

	public Text GameInfoText {get; set;}

	#endregion

	#region Lifecycle

	protected override void Awake() 
	{
		base.Awake();

		float barPositionY = 86.0f;

		ShiftBarBackgroundImage = AddNewChildImage("ShiftBarBackgroundImage", "Sprites/ShiftBarBackground");
		SetFrameForRectTransform(ShiftBarBackgroundImage.rectTransform, 512.0f, barPositionY, 256, 64);

		ShiftBarRefill = AddNewChildImage("ShiftBarRefill", "Sprites/ShiftBarRefill");
		SetFrameForRectTransform(ShiftBarRefill.rectTransform, 512.0f, barPositionY, 256, 64);
		ShiftBarRefill.fillMethod = Image.FillMethod.Horizontal;
		ShiftBarRefill.type = Image.Type.Filled;
		ShiftBarRefill.fillAmount = 0;

		ShiftBarFill = AddNewChildImage("ShiftBarFill", "Sprites/ShiftBarFill");
		SetFrameForRectTransform(ShiftBarFill.rectTransform, 512.0f, barPositionY, 256, 64);
		ShiftBarFill.fillMethod = Image.FillMethod.Horizontal;
		ShiftBarFill.type = Image.Type.Filled;
		ShiftBarFill.fillAmount = 1;
		ShiftBarFill.gameObject.SetActive(false);

		float textPositionX = 512.0f;
		float textPositionY = 32.0f;
		float textWith = 512.0f;
		float textHeight = 64.0f;
		SheepStatusText = AddNewChildText(
			"SheepStatusText",
			"Feeling Sheepish",
			"Fonts/DCC - Cloud",
			36,
			Color.white,
			TextAnchor.MiddleCenter);

		Outline sheepTextOutline = SheepStatusText.gameObject.AddComponent<Outline>();
		sheepTextOutline.effectColor = Color.black;
		sheepTextOutline.effectDistance = new Vector2(1,1);

		SetFrameForRectTransform(
			SheepStatusText.rectTransform,
			textPositionX,
			textPositionY,
			textWith,
			textHeight);

		ShiftStatusText = AddNewChildText(
			"SheepShiftTimerText",
			"Beast Mode!",
			"Fonts/Exquisite Corpse",
			36,
			new Color(0.66f, 0.27f, 0.27f),
			TextAnchor.MiddleCenter);
		
		SetFrameForRectTransform(
			ShiftStatusText.rectTransform,
			textPositionX,
			textPositionY,
			textWith,
			textHeight);

		Outline shiftTextOutline = SheepStatusText.gameObject.AddComponent<Outline>();
		shiftTextOutline.effectColor = Color.black;
		shiftTextOutline.effectDistance = new Vector2(1,1);

		ShiftStatusText.gameObject.SetActive(false);


		SheepEatenText = AddNewChildText(
			"SheepEatenText",
			"0 Sheep Eaten",
			"Fonts/DCC - Cloud",
			38,
			Color.red,
			TextAnchor.MiddleCenter);

		Outline sheepEatenTextOutline = SheepEatenText.gameObject.AddComponent<Outline>();
		sheepEatenTextOutline.effectColor = Color.black;
		sheepEatenTextOutline.effectDistance = new Vector2(1,1);
		SetFrameForRectTransform(
			SheepEatenText.rectTransform,
			textPositionX,
			768.0f - (textHeight/2),
			textWith,
			textHeight);

		TimeRemainingText = AddNewChildText(
			"TimeRemainingText",
			"",
			"Fonts/DCC - Cloud",
			38,
			Color.white,
			TextAnchor.MiddleCenter);

		Outline timeRemainingTextOutline = TimeRemainingText.gameObject.AddComponent<Outline>();
		timeRemainingTextOutline.effectColor = Color.black;
		timeRemainingTextOutline.effectDistance = new Vector2(1,1);
		SetFrameForRectTransform(
			TimeRemainingText.rectTransform,
			textPositionX,
			768.0f - (textHeight*1.5f),
			textWith,
			textHeight);


		GameInfoText = AddNewChildText(
			"GameInfoText",
			"",
			"Fonts/DCC - Cloud",
			38,
			Color.white,
			TextAnchor.MiddleCenter);

		Outline gameInfoTextOutline = GameInfoText.gameObject.AddComponent<Outline>();
		gameInfoTextOutline.effectColor = Color.black;
		gameInfoTextOutline.effectDistance = new Vector2(1,1);
		SetFrameForRectTransform(
			GameInfoText.rectTransform,
			textPositionX,
			(768.0f/2),
			1024.0f,
			textHeight*4);
	}

	// Use this for initialization
	protected override void Start() 
	{
		base.Start();
	}

	// Update is called once per frame
	protected override void Update() 
	{
		base.Update();
	}

	#endregion

	#region Timer Updates


	#endregion
}

