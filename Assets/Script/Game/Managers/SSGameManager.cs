﻿using UnityEngine;
using System.Collections;

public class SSGameManager : RDGameManager
{
	#region Singleton
	
	#endregion
	
	#region Manager Components
	
	#endregion
	
	#region Lifecycle
	
	protected override void Awake()
	{
		base.Awake();
		
		Application.targetFrameRate = 60;
	}
	
	// Use this for initialization
	protected override void Start()
	{
		base.Start();

		// Load content
		ContentManager.LoadAssetsFromManifest(SSContentConfig.MainContentManifest);
		
		// Load scene
		SceneManager.LoadScene(typeof(SSGameScene));
	}
	
	// Update is called once per frame
	protected override void Update()
	{
		base.Update();
	}
	
	#endregion
}