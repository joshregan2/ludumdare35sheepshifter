using UnityEngine;
using System.Collections;

public class SSBootstrapper : MonoBehaviour
{
	#region Fields and Properties
	
	#endregion
	
	#region Lifecycle
	
	protected void Awake()
	{
		EstablishGameManager();

		DontDestroyOnLoad(Camera.main.gameObject);
		Camera.main.orthographicSize = 5.12f / (float)Screen.width * (float)Screen.height;
		
		Destroy(gameObject);
	}
	
	// Use this for initialization
	protected void Start()
	{
		
	}
	
	// Update is called once per frame
	protected void Update()
	{
		
	}
	
	public void EstablishGameManager()
	{
		if (SSGameManager.SharedGameManager == null)
		{
			GameObject gameManager = new GameObject("Game Manager");
			gameManager.AddComponent<SSGameManager>();
		}
	}
	
	#endregion
}

