using UnityEngine;
using System.Collections;

public class SSGameConfig
{
	// Physics
	public const int PHYSICS_LAYER_PLAYER = 8;
	public const int PHYSICS_LAYER_FENCES = 9;
	public const int PHYSICS_LAYER_SHEEP = 10;
}

