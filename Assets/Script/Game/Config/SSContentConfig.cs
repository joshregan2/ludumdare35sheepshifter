using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SSContentConfig
{
	public static readonly List<string> MainContentManifest = new List<string>()
	{
		"Prefabs/BackgroundTilePrefab",
		"Prefabs/SheepPrefab",
		"Prefabs/WolfPrefab",
		"Prefabs/BottomLeftPrefab",
		"Prefabs/BottomMidPrefab",
		"Prefabs/BottomRightPrefab",
		"Prefabs/MidLeftPrefab",
		"Prefabs/MidRightPrefab",
		"Prefabs/TopLeftPrefab",
		"Prefabs/TopMidPrefab",
		"Prefabs/TopRightPrefab",
		"Prefabs/Mid1Prefab",
		"Prefabs/Mid2Prefab",
		"Prefabs/Mid3Prefab",
		"Prefabs/SheepCorpse1Prefab",
		"Prefabs/SheepCorpse2Prefab",
		"Prefabs/SheepCorpse3Prefab",
		"Prefabs/GibParticlePrefab",
		"Prefabs/BushPrefab"
	};
}

