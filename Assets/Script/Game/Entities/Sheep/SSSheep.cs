﻿using UnityEngine;
using System.Collections;

public class SSSheep : SSEntity 
{
	#region Fields and Properties

	public const string ANIM_KEY_IDLE = "SheepIdle";
	public const string ANIM_KEY_WALK = "SheepWalk";
	public const string ANIM_KEY_RUN = "SheepRun";
	public const string ANIM_KEY_SCARED = "SheepScared";

	public const float MIN_DECISION_TIME = 2.0f;
	public const float MAX_DECISION_TIME = 8.0f;
	public const float MOVE_SPEED = 0.02f;
	public const float ESCAPE_SPEED = 0.25f;

	public const float ESCAPE_DISTANCE = 10.0f;

	protected Vector2 currentMovement;

	protected float decisionTimer;
	protected float timeUntilNextDecision;

	public enum SheepActions
	{
		Idle,
		Move,
		ActionCount
	};
	public SheepActions CurrentAction {get; set;}
	public bool IsEscaping {get; protected set;}

	public SSGameScene ParentScene {get; set;}

	public Animator Animator {get; set;}

	#endregion

	#region Lifecycle

	protected override void Awake()
	{
		base.Awake();

		IsEscaping = false;
		currentMovement = Vector2.zero;
		decisionTimer = 0;
		timeUntilNextDecision = Random.Range(MIN_DECISION_TIME, MAX_DECISION_TIME);
		CurrentAction = SheepActions.Idle;

		AddRigidBodyAndCollider();
		PhysicsBody.gravityScale = 0;
		PhysicsBody.freezeRotation = true;
		PhysicsBody.drag = 4.0f;
		MainCollider.radius = RDNode.ConvertPixelToUnit(15.0f);
		MainCollider.offset = new Vector2(0.01f, 0);
		gameObject.layer = SSGameConfig.PHYSICS_LAYER_SHEEP;
	}

	// Use this for initialization
	protected override void Start()
	{
		base.Start();

		Animator = gameObject.GetComponent<Animator>();
	}

	// Update is called once per frame
	protected override void Update()
	{
		base.Update();

		if (ParentScene != null && ParentScene.Player != null &&
			ParentScene.Player.IsInWolfMode == true)
		{
			IsEscaping = true;
		}
		else
		{
			IsEscaping = false;
		}

		if (IsEscaping == false)
		{
			CheckForDecision();

			MoveInDirection(currentMovement);
		}
		else
		{
			EscapeFromWolf();
		}

		UpdateFacing();
	}

	#endregion

	#region Sheep Brain

	protected void CheckForDecision()
	{
		if (decisionTimer < timeUntilNextDecision)
		{
			decisionTimer += Time.deltaTime;
		}
		else
		{
			decisionTimer = 0;
			timeUntilNextDecision = Random.Range(MIN_DECISION_TIME, MAX_DECISION_TIME);

			MakeDecision();
		}
	}

	protected void MakeDecision()
	{
		int randomDecision = Random.Range(0, 100);

		if (randomDecision <= 60)
		{
			CurrentAction = SheepActions.Idle;
		}
		else
		{
			CurrentAction = SheepActions.Move;
		}

		switch(CurrentAction)
		{
		case SheepActions.Idle:
			currentMovement = Vector2.zero;
			break;
		case SheepActions.Move:
			ChooseMovement();
			break;
		}

		RunAnimationForAction();
	}

	public void RunAnimationForAction()
	{
		switch(CurrentAction)
		{
		case SheepActions.Idle:
			Animator.StopPlayback();
			Animator.Play(ANIM_KEY_IDLE);
			break;
		case SheepActions.Move:
			Animator.StopPlayback();
			Animator.Play(ANIM_KEY_WALK);
			break;
		}
	}

	#endregion

	#region Movement

	public void UpdateFacing()
	{
		if (PhysicsBody.velocity.x > 0)
		{
			gameObject.transform.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
		}
		else if (PhysicsBody.velocity.x < 0)
		{
			gameObject.transform.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
		}
	}

	public void ChooseMovement()
	{
		int randX = Random.Range(0, 10);
		int randY = Random.Range(0, 10);

		float velocityX = 0;
		float velocityY = 0;

		// Up and down
		if (randY < 5)
		{
			velocityY = 1;
		}
		else
		{
			velocityY = -1;
		}

		// Left and right
		if (randX < 5)
		{
			velocityX = -1;
		}
		else
		{
			velocityX = 1;
		}

		currentMovement = new Vector2(velocityX, velocityY);
	}

	public void MoveInDirection(Vector2 direction)
	{
		if (direction.x != 0 || direction.y != 0)
		{
			PhysicsBody.AddForce(direction.normalized * MOVE_SPEED, ForceMode2D.Impulse);
		}
	}

	public void EscapeFromWolf()
	{
//		if (Animator.GetCurrentAnimatorStateInfo(0).IsName(ANIM_KEY_RUN) == false)
//		{
//			Animator.Play(ANIM_KEY_RUN);
//		}

		// Escape from wolf
		if (ParentScene != null && ParentScene.Player != null &&
			ParentScene.Player.IsInWolfMode == true)
		{
			Vector3 wolfPosition = ParentScene.Player.gameObject.transform.position;

			if (Vector3.Distance(wolfPosition, gameObject.transform.position) <= ESCAPE_DISTANCE)
			{
				Vector3 inverseDirectionToWolf = gameObject.transform.position - wolfPosition;
				Vector2 direction = new Vector2(inverseDirectionToWolf.x, inverseDirectionToWolf.y); 

				PhysicsBody.AddForce(direction.normalized * ESCAPE_SPEED, ForceMode2D.Impulse);
			}

			// Avoid bounds
			float innerBounds = 2.0f;
			float additionalMovementX = 0;
			float additionalMovementY = 0;
			if (gameObject.transform.position.y <= ParentScene.BoundsMinY + innerBounds)
			{
				additionalMovementY = 1;
			}
			else if (gameObject.transform.position.y >= ParentScene.BoundsMaxY - innerBounds)
			{
				additionalMovementY = -1;
			}

			if (gameObject.transform.position.x <= ParentScene.BoundsMinX + innerBounds)
			{
				additionalMovementX = 1;
			}
			else if (gameObject.transform.position.x >= ParentScene.BoundsMaxX - innerBounds)
			{
				additionalMovementX = -1;
			}

			PhysicsBody.AddForce((new Vector2(additionalMovementX, additionalMovementY)).normalized * ESCAPE_SPEED, ForceMode2D.Impulse);
		
			float obstacleAvoidRadius = 1.0f;
			for (int i = 0; i < ParentScene.AllObstacles.Count; i++)
			{
				if (Vector3.Distance(ParentScene.AllObstacles[i].transform.position, gameObject.transform.position) <= obstacleAvoidRadius)
				{
					Vector3 inverseDirectionToObstacle = gameObject.transform.position - ParentScene.AllObstacles[i].transform.position;
					Vector2 avoidDirection = new Vector2(inverseDirectionToObstacle.x, inverseDirectionToObstacle.y); 

					PhysicsBody.AddForce(avoidDirection.normalized * (ESCAPE_SPEED*1.1f), ForceMode2D.Impulse);
				}
			}
		}
	}

	#endregion

	#region Collision

	protected void OnCollisionEnter2D(Collision2D collision) 
	{
		if (collision.gameObject.layer == SSGameConfig.PHYSICS_LAYER_PLAYER)
		{
			if (ParentScene != null && ParentScene.Player != null &&
				ParentScene.Player.IsInWolfMode == true)
			{
				DeathByWolf();
			}
		}
	}

	protected void DeathByWolf()
	{
		if (ParentScene != null)
		{
			ParentScene.HandleSheepDeath(this);
		}

		Destroy(this.gameObject);
	}

	#endregion
}

