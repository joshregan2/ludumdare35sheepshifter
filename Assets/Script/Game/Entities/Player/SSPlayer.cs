﻿using UnityEngine;
using System.Collections;
using InControl;

public class SSPlayer : SSEntity 
{
	#region Fields and Properties

	public const string ANIM_KEY_IDLE = "WolfIdle";
	public const string ANIM_KEY_RUN = "WolfRun";

	public const float SHEEP_SHIFT_DURATION = 6.0f;
	public const float WOLF_SHIFT_DURATION = 12.0f;

	public const float SHEEP_MOVE_SPEED = 0.05f;
	public const float WOLF_MOVE_SPEED = 0.25f;
	public const float CAMERA_FOLLOW_SPEED = 8.0f;

	private bool readyToWolf;
	public bool IsInWolfMode {get; set;}

	public float ShiftTimer {get; protected set;}

	public SSGameScene ParentScene {get; set;}

	public Animator Animator {get; set;}

	public SpriteRenderer spriteRenderer;

	private Color originalTint;
	private Color sheepTint;

	#endregion
	
	#region Lifecycle
	
	protected override void Awake()
	{
		base.Awake();

		readyToWolf = false;
		IsInWolfMode = false;
		ShiftTimer = 0;

		AddRigidBodyAndCollider();
		PhysicsBody.gravityScale = 0;
		PhysicsBody.freezeRotation = true;
		PhysicsBody.drag = 1.0f;
		gameObject.layer = SSGameConfig.PHYSICS_LAYER_PLAYER;

		MainCollider.radius = RDNode.ConvertPixelToUnit(24.0f);

		sheepTint = new Color(1.0f, 0.7f, 0.7f);
	}
	
	// Use this for initialization
	protected override void Start()
	{
		base.Start();

		Animator = gameObject.GetComponent<Animator>();
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		originalTint = spriteRenderer.color;
		spriteRenderer.color = sheepTint;
	}
	
	// Update is called once per frame
	protected override void Update()
	{
		base.Update();

		HandleInput();

		UpdateFacing();
	}

	protected void FixedUpdate()
	{
		UpdateCameraFollowPosition();
	}

	#endregion

	#region Camera

	protected void UpdateCameraFollowPosition()
	{
		if (ParentScene != null & ParentScene.CameraParent != null)
		{
			GameObject cameraObject = Camera.main.gameObject;
			
			Vector3 newCameraPosition = Vector3.Lerp(
				cameraObject.transform.position,
				new Vector3(
					Mathf.Clamp(this.gameObject.transform.position.x, ParentScene.BoundsMinX, ParentScene.BoundsMaxX),
					Mathf.Clamp(this.gameObject.transform.position.y, ParentScene.BoundsMinY, ParentScene.BoundsMaxY),
					cameraObject.transform.position.z),
				Time.deltaTime * CAMERA_FOLLOW_SPEED);
			
			cameraObject.transform.position = newCameraPosition;
		}
	}

	#endregion

	#region Input

	public void UpdateFacing()
	{
		if (PhysicsBody.velocity.x > 0)
		{
			gameObject.transform.transform.localRotation = Quaternion.Euler(new Vector3(0, IsInWolfMode == false ? 180 : 0, 0));
		}
		else if (PhysicsBody.velocity.x < 0)
		{
			gameObject.transform.transform.localRotation = Quaternion.Euler(new Vector3(0, IsInWolfMode == false ? 0 : 180, 0));
		}
	}

	protected void HandleInput()
	{
		if (ParentScene != null && ParentScene.CurrentState == SSGameScene.GameStates.GamePlaying)
		{
			HandlePlayerMovement();

			if (readyToWolf == true && IsInWolfMode == false && (Input.GetKeyDown(KeyCode.Space) ||
				(InputManager.Devices.Count > 0 && 
				InputManager.Devices[0].LeftTrigger.WasPressed))) // Wolf the fuck out!
			{
				ShiftTimer = 0;
				EnableWolfMode(true);
			}

			CheckShiftTimer();
		}
	}

	public void HandlePlayerMovement()
	{
		float velocityX = 0;
		float velocityY = 0;

		if (InputManager.Devices.Count > 0)
		{
			velocityX = InputManager.Devices[0].LeftStick.X;
			velocityY = InputManager.Devices[0].LeftStick.Y;
		}

		// Up and down
		if (Input.GetKey(KeyCode.W))
		{
			velocityY = 1;
		}
		else if (Input.GetKey(KeyCode.S))
		{
			velocityY = -1;
		}

		// Left and right
		if (Input.GetKey(KeyCode.A))
		{
			velocityX = -1;
		}
		else if (Input.GetKey(KeyCode.D))
		{
			velocityX = 1;
		}

		Vector2 movement = new Vector2(velocityX, velocityY);

		if (movement.x != 0 || movement.y != 0)
		{
			float currentSpeed = IsInWolfMode == true ? WOLF_MOVE_SPEED : SHEEP_MOVE_SPEED;
			PhysicsBody.AddForce(movement.normalized * currentSpeed, ForceMode2D.Impulse);
		}


		UpdateAnimation(movement);
	}

	protected void UpdateAnimation(Vector2 movement)
	{
		Animator.StopPlayback();
		if (IsInWolfMode == true)
		{
			if (movement == Vector2.zero)
			{
				Animator.Play(ANIM_KEY_IDLE);
			}
			else
			{
				Animator.Play(ANIM_KEY_RUN);
			}
		}
		else
		{
			if (movement == Vector2.zero)
			{
				Animator.Play(SSSheep.ANIM_KEY_IDLE);
			}
			else
			{
				Animator.Play(SSSheep.ANIM_KEY_WALK);
			}
		}
	}

	#endregion

	#region Mode Change

	public void CheckShiftTimer()
	{
		if (IsInWolfMode == false)
		{
			if (readyToWolf == false)
			{
				if (ShiftTimer < SHEEP_SHIFT_DURATION)
				{
					ShiftTimer += Time.deltaTime;
				}
				else
				{
					ShiftTimer = 0;
					readyToWolf = true;
					if (ParentScene != null)
					{
						ParentScene.HandleWolfReady();
					}
				}
			}
		}
		else
		{
			if (ShiftTimer < WOLF_SHIFT_DURATION)
			{
				ShiftTimer += Time.deltaTime;
			}
			else
			{
				ShiftTimer = 0;
				EnableWolfMode(false);
				if (ParentScene != null)
				{
					ParentScene.HandleWolfEnd();
				}
			}
		}
	}

	public void EnableWolfMode(bool enable)
	{
		IsInWolfMode = enable;
		if (ParentScene != null)
		{
			if (enable == true)
			{
				spriteRenderer.color = originalTint;
				readyToWolf = false;
				ParentScene.HandleWolfOut();
			}
		}
		if (enable == false)
		{
			readyToWolf = false;
			spriteRenderer.color = sheepTint;
		}
	}

	#endregion
}
