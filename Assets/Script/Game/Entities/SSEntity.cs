﻿using UnityEngine;
using System.Collections;

public class SSEntity : RDNode 
{
	#region Fields and Properties

	public Rigidbody2D PhysicsBody {get; set;}
	public CircleCollider2D MainCollider {get; set;}

	#endregion

	#region Lifecycle

	protected override void Awake()
	{
		base.Awake();

	}

	// Use this for initialization
	protected override void Start()
	{
		base.Start();


	}

	// Update is called once per frame
	protected override void Update()
	{
		base.Update();


	}

	protected void AddRigidBodyAndCollider() 
	{
		if (PhysicsBody == null)
		{
			PhysicsBody = gameObject.AddComponent<Rigidbody2D>();
		}

		if (MainCollider == null)
		{
			MainCollider = gameObject.AddComponent<CircleCollider2D>();
		}
	}

	#endregion

	#region Movement


	#endregion
}

