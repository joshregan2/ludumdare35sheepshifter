using UnityEngine;
using System.Collections;

public class SSScene : RDScene
{
	#region Fields and Properties
	
	#endregion
	
	#region Lifecycle
	
	protected override void Awake()
	{
		base.Awake();
		
	}
	
	// Use this for initialization
	protected override void Start()
	{
		base.Start();
	}
	
	// Update is called once per frame
	protected override void Update()
	{
		base.Update();
	}
	
	public override void Shutdown()
	{
		// Run any shutdown logic, remove components etc.
	}
	
	#endregion
	
	#region Scene Loading
	
	protected override void LoadSceneObjects()
	{
		base.LoadSceneObjects();
	}
	
	#endregion
}

