using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using InControl;

public class SSGameScene : SSScene
{
	#region Fields and Properties

	public const float GAME_TIME_SECS = 10;

	public const int TOTAL_AREA_SIZE = 4;
	public const int TOTAL_SHEEP = 80;
	public const int TOTAL_BUSHES = 20;

	public float BoundsMinX {get; set;}
	public float BoundsMinY {get; set;}
	public float BoundsMaxX {get; set;}
	public float BoundsMaxY {get; set;}

	public float BgTileUnitWidth {get; set;}
	public float BgTileUnitHeight {get; set;}

	public RDCanvas MainCanvas {get; set;}
	public SSMainGameView MainGameView {get; set;}

	public List<GameObject> BackgroundTiles {get; set;}
	public List<GameObject> Boundaries {get; set;}

	public SSPlayer Player {get; set;}

	public List<SSSheep> AllSheep {get; set;}
	public List<GameObject> AllObstacles {get; set;}

	public int PlayerPoints {get; set;}

	public GameObject CameraParent {get; set;}

	private bool isCameraShaking;

	private float gameTimer;

	public enum GameStates
	{
		GameStart,
		GamePlaying,
		GameEnd
	};
	public GameStates CurrentState {get; set;}

	#endregion
	
	#region Lifecycle
	
	protected override void Awake()
	{
		// Load UI
		MainCanvas = new RDCanvas("UI Canvas", 1024, 768);
		MainGameView = (SSMainGameView)MainCanvas.CreateNewScreen(typeof(SSMainGameView), "MainGameView");
		MainCanvas.PushViewToCanvas(MainGameView);

		MainGameView.GameInfoText.text = "Use WASD or left analogue stick to move\nSpace or Left Trigger to shapeshift."+
			"\n\nPress Space or Left Trigger to play!";

		base.Awake();

		gameTimer = 0;
		isCameraShaking = false;
		CameraParent = GameObject.Find("Camera");
		CurrentState = GameStates.GameStart;
	}
	
	// Use this for initialization
	protected override void Start()
	{
		base.Start();
	}
	
	// Update is called once per frame
	protected override void Update()
	{
		base.Update();

		UpdateShiftBars();

		UpdateTimer();

		HandleInput();
	}
	
	protected void FixedUpdate()
	{
	}
	
	public override void Shutdown()
	{
		// Run any shutdown logic, remove components etc.
	}
	
	#endregion
	
	#region Scene Loading
	
	protected override void LoadSceneObjects()
	{
		base.LoadSceneObjects();

		// Load Background
		BackgroundTiles = new List<GameObject>();
		float bgTilePixelWidth = 1024;
		float bgTilePixelHeight = 768;

		BgTileUnitWidth = RDNode.ConvertPixelToUnit(bgTilePixelWidth);
		BgTileUnitHeight = RDNode.ConvertPixelToUnit(bgTilePixelHeight);

		int totalTilesX = TOTAL_AREA_SIZE;
		int totalTilesY = totalTilesX;

		BoundsMinX = -((BgTileUnitWidth/2.0f) + (BgTileUnitWidth*((totalTilesX/2)-1)));
		BoundsMinY = -((BgTileUnitHeight/2.0f) + (BgTileUnitHeight*((totalTilesY/2)-1)));
		BoundsMaxX = BoundsMinX + (BgTileUnitWidth*(totalTilesX-1));
		BoundsMaxY = BoundsMinY + (BgTileUnitHeight*(totalTilesY-1));

		for (int y = 0; y < totalTilesY; y++)
		{
			for (int x = 0; x < totalTilesX; x++)
			{
				string prefabKey = "BackgroundTilePrefab";

				if (x == 0 && y == 0)
				{
					prefabKey = "BottomLeftPrefab";
				}
				else if (x == 0 && y > 0 && y < totalTilesY - 1)
				{
					prefabKey = "MidLeftPrefab";
				}
				else if (x == totalTilesX - 1 && y > 0 && y < totalTilesY-1)
				{
					prefabKey = "MidRightPrefab";
				}
				else if (x == totalTilesX - 1 && y == 0)
				{
					prefabKey = "BottomRightPrefab";
				}
				else if (x == 0 && y == totalTilesY-1)
				{
					prefabKey = "TopLeftPrefab";
				}
				else if (x == totalTilesX - 1 && y == totalTilesY-1)
				{
					prefabKey = "TopRightPrefab";
				}
				else if (x > 0 && x < totalTilesX - 1 && y == 0)
				{
					prefabKey = "BottomMidPrefab";
				}
				else if (x > 0 && x < totalTilesX - 1 && y == totalTilesY-1)
				{
					prefabKey = "TopMidPrefab";
				}
				else
				{
					int random = Random.Range(0, 2);
					switch(random)
					{
					case 0:
						prefabKey = "Mid1Prefab";
						break;
					case 1:
						prefabKey = "Mid2Prefab";
						break;
					case 2:
						prefabKey = "Mid3Prefab";
						break;
					}
				}

				GameObject backgroundTile = SSGameManager.SharedGameManager.ContentManager.GetCloneOfPrefab(prefabKey);
				backgroundTile.transform.position = new Vector3(
					BoundsMinX + (BgTileUnitWidth*x),
					BoundsMinY + (BgTileUnitHeight*y),
					0);

				backgroundTile.transform.parent = this.gameObject.transform;
				BackgroundTiles.Add(backgroundTile);
			}
		}

		SetupGameBoundaries();

		// Load bushes
		AllObstacles = new List<GameObject>();
		int bushCount = TOTAL_BUSHES;
		float bushSizeHalf = RDNode.ConvertPixelToUnit(32.0f);
		for (int i = 0; i < bushCount; i++)
		{
			Vector2 bushPosition = new Vector2(
				Random.Range(BoundsMinX+bushSizeHalf, BoundsMaxX-bushSizeHalf),
				Random.Range(BoundsMinY+bushSizeHalf, BoundsMaxY-bushSizeHalf));

			SpawnBush(bushPosition);
		}

		// Load player
		SpawnPlayer(Vector2.zero);


		// Load sheep
		AllSheep = new List<SSSheep>();
		int sheepCount = TOTAL_SHEEP;
		float sheepSizeHalf = RDNode.ConvertPixelToUnit(32.0f);
		for (int i = 0; i < sheepCount; i++)
		{
			Vector2 sheepPosition = new Vector2(
				Random.Range(BoundsMinX+sheepSizeHalf, BoundsMaxX-sheepSizeHalf),
				Random.Range(BoundsMinY+sheepSizeHalf, BoundsMaxY-sheepSizeHalf));

			SpawnSheep(sheepPosition);
		}

		ResetGame();
	}

	protected void SetupGameBoundaries()
	{
		Boundaries = new List<GameObject>();
		float boundaryThickness = 1;

		for (int i = 0; i < 4; i++)
		{
			GameObject boundary = new GameObject("Boundary");
			BoxCollider2D boxCollider = boundary.AddComponent<BoxCollider2D>();

			Vector2 size = Vector2.zero;
			Vector2 position = Vector2.zero;
			switch(i)
			{
			case 0: // Top
				size = new Vector2((BoundsMaxX - BoundsMinX)+BgTileUnitWidth, boundaryThickness);
				position = new Vector2(0, BoundsMaxY+(BgTileUnitHeight/2));
				break;
			case 1: // Right
				size = new Vector2(boundaryThickness, (BoundsMaxY - BoundsMinY)+BgTileUnitHeight);
				position = new Vector2(BoundsMaxX+(BgTileUnitWidth/2), 0);
				break;
			case 2: // Down
				size = new Vector2((BoundsMaxX - BoundsMinX)+BgTileUnitWidth, boundaryThickness);
				position = new Vector2(0, BoundsMinY-(BgTileUnitHeight/2));
				break;
			case 3: // Left
				size = new Vector2(boundaryThickness, (BoundsMaxY - BoundsMinY)+BgTileUnitHeight);
				position = new Vector2(BoundsMinX-(BgTileUnitWidth/2), 0);
				break;
			}
			boxCollider.size = size;
			boundary.transform.position = new Vector3(position.x, position.y, 0);
			boundary.transform.parent = this.gameObject.transform;
			boundary.layer = SSGameConfig.PHYSICS_LAYER_FENCES;
			Boundaries.Add(boundary);
		}
	}

	protected void SpawnPlayer(Vector2 position)
	{
		GameObject playerGameObject = SSGameManager.SharedGameManager.ContentManager.GetCloneOfPrefab("WolfPrefab");
		Player = playerGameObject.AddComponent<SSPlayer>();
		Player.gameObject.transform.position = new Vector3(position.x, position.y, 0);
		Player.gameObject.transform.parent = this.gameObject.transform;
		Player.ParentScene = this;
	}

	protected void SpawnSheep(Vector2 position)
	{
		GameObject sheepGameObject = SSGameManager.SharedGameManager.ContentManager.GetCloneOfPrefab("SheepPrefab");
		SSSheep sheep = sheepGameObject.AddComponent<SSSheep>();
		sheep.gameObject.transform.position = new Vector3(position.x, position.y, 0);
		sheep.gameObject.transform.parent = this.gameObject.transform;

		sheep.ParentScene = this;
		AllSheep.Add(sheep);
	}

	protected void SpawnBush(Vector2 position)
	{
		GameObject bushGameObject = SSGameManager.SharedGameManager.ContentManager.GetCloneOfPrefab("BushPrefab");
		CircleCollider2D bushCollider = bushGameObject.AddComponent<CircleCollider2D>();
		bushCollider.radius = RDNode.ConvertPixelToUnit(14.0f);
		bushGameObject.transform.position = new Vector3(position.x, position.y, 0);
		bushGameObject.transform.parent = this.gameObject.transform;
		bushGameObject.layer = SSGameConfig.PHYSICS_LAYER_FENCES;

		AllObstacles.Add(bushGameObject);
	}

	#endregion

	#region Game Logic

	protected override void HandleInput()
	{
		if (CurrentState == GameStates.GameStart)
		{
			if (Input.GetKeyDown(KeyCode.Space) ||
				(InputManager.Devices.Count > 0 && 
				InputManager.Devices[0].LeftTrigger.WasPressed))
			{
				ResetGame();
				StartGame();
			}
		}
		else if (CurrentState == GameStates.GameEnd)
		{
			if (Input.GetKeyDown(KeyCode.Space) ||
				(InputManager.Devices.Count > 0 && 
					InputManager.Devices[0].LeftTrigger.WasPressed))
			{
				ResetGame();
				Reload();
				StartGame();
			}
		}
	}

	protected void ResetGame()
	{
		PlayerPoints = 0;
		gameTimer = GAME_TIME_SECS;
		if (MainGameView != null && MainGameView.TimeRemainingText != null)
		{
			MainGameView.TimeRemainingText.text = ((int)gameTimer).ToString() + " Seconds Remaining";
		}
		MainGameView.SheepStatusText.gameObject.SetActive(true);
		MainGameView.ShiftStatusText.gameObject.SetActive(false);

		MainGameView.ShiftBarRefill.gameObject.SetActive(true);
		MainGameView.ShiftBarRefill.fillAmount = 0;

		MainGameView.ShiftBarFill.gameObject.SetActive(false);
		MainGameView.ShiftBarFill.fillAmount = 1;
	}

	protected void Reload()
	{
		DestroyAllChildren();
		LoadSceneObjects();
	}

	protected void StartGame()
	{
		CurrentState = GameStates.GamePlaying;
		MainGameView.GameInfoText.gameObject.SetActive(false);;
	}

	protected void HandleGameOver()
	{
		CurrentState = GameStates.GameEnd;
		MainGameView.GameInfoText.gameObject.SetActive(true);
		MainGameView.GameInfoText.text = "The beast must rest... "+PlayerPoints.ToString()+
			" sheep eaten\nPress Space or Left Trigger to play again.";
	}

	protected void UpdateTimer()
	{
		if (CurrentState == GameStates.GamePlaying)
		{
			if (gameTimer > 0)
			{
				gameTimer -= Time.deltaTime;
			}
			else
			{
				gameTimer = 0;
				HandleGameOver();
			}

			MainGameView.TimeRemainingText.text = ((int)gameTimer).ToString() + " Seconds Remaining";
		}
	}

	public void SpawnSheepOutsideOfCameraRect()
	{
		bool spawnRight = Camera.main.gameObject.transform.position.x < 0 ? true : false;
		bool spawnAbove = Camera.main.gameObject.transform.position.y < 0 ? true : false; 

		float screenAspect = (float)Screen.width / (float)Screen.height;
		float cameraHeight = Camera.main.orthographicSize * 2;
		Bounds bounds = new Bounds(
			Camera.main.gameObject.transform.position,
			new Vector3(cameraHeight * screenAspect, cameraHeight, 0));

		float minX = 0;
		float minY = 0;
		float maxX = 0;
		float maxY = 0;

		if (spawnRight == true)
		{
			minX = bounds.center.x + bounds.extents.x;
			maxX = minX + bounds.extents.x;
		}
		else
		{
			minX = bounds.center.x - bounds.size.x;
			maxX = minX + bounds.extents.x;
		}

		if (spawnAbove == true)
		{
			minY = bounds.center.y + bounds.extents.y;
			maxY = minY + bounds.extents.y;
		}
		else
		{
			minY = bounds.center.y - bounds.size.y;
			maxX = minY + bounds.extents.y;
		}

		Vector2 sheepPosition = new Vector2(
			Random.Range(minX, maxX),
			Random.Range(minY, maxY));

		SpawnSheep(sheepPosition);
	}

	public void HandleSheepDeath(SSSheep sheep)
	{
		AllSheep.Remove(sheep);

		Vector3 deathPosition = sheep.gameObject.transform.position;

		ShowSheepSlaughterAtPosition(deathPosition, false);

		PlayerPoints += 1;
		MainGameView.SheepEatenText.text = PlayerPoints.ToString() + " Sheep Eaten";

		if (InputManager.Devices.Count > 0)
		{
			InputManager.Devices[0].Vibrate(0.7f);
		}

		SpawnSheepOutsideOfCameraRect();

		ShakeCamera(0.1f, 0.2f);
	}

	public void ShowSheepSlaughterAtPosition(Vector3 position, bool isTinted)
	{
		Color tintColour = new Color(1.0f, 0.7f, 0.7f);
		for (int i = 0; i < 3; i++)
		{
			string prefabKey = "";
			switch(i)
			{
			case 0:
				prefabKey = "SheepCorpse1Prefab";
				break;
			case 1:
				prefabKey = "SheepCorpse2Prefab";
				break;
			case 2:
				prefabKey = "SheepCorpse3Prefab";
				break;
			}

			GameObject corpseGameObject = SSGameManager.SharedGameManager.ContentManager.GetCloneOfPrefab(prefabKey);
			corpseGameObject.transform.position = position;
			corpseGameObject.transform.parent = this.gameObject.transform;

			if (isTinted == true)
			{
				SpriteRenderer spriteRenderer = corpseGameObject.GetComponent<SpriteRenderer>();
				spriteRenderer.color = tintColour;
			}

			Rigidbody2D corpseBody = corpseGameObject.AddComponent<Rigidbody2D>();
			corpseBody.gravityScale = 0;
			corpseBody.drag = 0.5f;

			float moveRange = 2.0f;
			corpseBody.AddForce(new Vector2(
				Random.Range(-moveRange, moveRange),
				Random.Range(-moveRange, moveRange)), ForceMode2D.Impulse);

			corpseGameObject.AddComponent<RDAutoDestroyRigidBody2D>();

		}

		GameObject particleEffectGameObject = SSGameManager.SharedGameManager.ContentManager.GetCloneOfPrefab("GibParticlePrefab");
		particleEffectGameObject.AddComponent<RDAutoDestroyParticleSystem>();
		particleEffectGameObject.transform.position = position;
		particleEffectGameObject.transform.parent = gameObject.transform;
	}

	public void HandleWolfReady()
	{
		MainGameView.ShiftBarFill.fillAmount = 1.0f;
		MainGameView.ShiftBarFill.gameObject.SetActive(true);
		MainGameView.ShiftBarRefill.gameObject.SetActive(false);

		MainGameView.SheepStatusText.text = "The beast is ready...";

		if (InputManager.Devices.Count > 0)
		{
			InputManager.Devices[0].Vibrate(0.1f);
		}
	}

	public void HandleWolfOut()
	{
		ShowSheepSlaughterAtPosition(Player.gameObject.transform.position, true);

		for (int i = 0; i < AllSheep.Count; i++)
		{
			AllSheep[i].Animator.Play(SSSheep.ANIM_KEY_SCARED);
		}

		MainGameView.ShiftBarFill.fillAmount = 1.0f;
		MainGameView.ShiftBarFill.gameObject.SetActive(true);
		MainGameView.ShiftBarRefill.gameObject.SetActive(false);

		MainGameView.ShiftStatusText.gameObject.SetActive(true);
		MainGameView.SheepStatusText.gameObject.SetActive(false);

		if (InputManager.Devices.Count > 0)
		{
			InputManager.Devices[0].Vibrate(0.6f);
		}

		ShakeCamera(0.2f, 0.4f);
	}

	public void HandleWolfEnd()
	{
		MainGameView.SheepStatusText.text = "Feeling sheepish";

		for (int i = 0; i < AllSheep.Count; i++)
		{
			AllSheep[i].RunAnimationForAction();
		}

		if (InputManager.Devices.Count > 0)
		{
			InputManager.Devices[0].Vibrate(0.4f);
		}
	}

	public void UpdateShiftBars()
	{
		float targetTime = Player.IsInWolfMode == false ? SSPlayer.SHEEP_SHIFT_DURATION : SSPlayer.WOLF_SHIFT_DURATION;
		float percentageTimerCompletion = Player.ShiftTimer / targetTime;
		percentageTimerCompletion = Mathf.Clamp(percentageTimerCompletion, 0, 1.0f);

		if (Player.IsInWolfMode == false)
		{
			MainGameView.SheepStatusText.gameObject.SetActive(true);
			MainGameView.ShiftStatusText.gameObject.SetActive(false);

			MainGameView.ShiftBarRefill.gameObject.SetActive(true);
			MainGameView.ShiftBarRefill.fillAmount = percentageTimerCompletion;
		}
		else
		{
			MainGameView.ShiftStatusText.gameObject.SetActive(true);
			MainGameView.SheepStatusText.gameObject.SetActive(false);

			MainGameView.ShiftBarFill.gameObject.SetActive(true);
			MainGameView.ShiftBarRefill.gameObject.SetActive(false);
			MainGameView.ShiftBarFill.fillAmount = 1.0f - percentageTimerCompletion;
		}
	}

	public void ShakeCamera(float duration, float intensity)
	{
		if (isCameraShaking == false)
		{
			isCameraShaking = true;
			iTween.ShakePosition(CameraParent, iTween.Hash(
				"time", duration,
				"x", intensity,
				"y", intensity,
				"oncompletetarget", gameObject,
				"oncomplete", "CameraShakeComplete"
			));
		}
	}

	protected void CameraShakeComplete()
	{
		isCameraShaking = false;
	}

	#endregion
}