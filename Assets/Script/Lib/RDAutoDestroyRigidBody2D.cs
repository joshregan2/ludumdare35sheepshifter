﻿using UnityEngine;
using System.Collections;

public class RDAutoDestroyRigidBody2D : MonoBehaviour
{
	#region Fields and Properties

	private Rigidbody2D body;

	#endregion

	#region Lifecycle

	// Use this for initialization
	void Start ()
	{
		body = this.gameObject.GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update ()
	{
		if (body != null && body.velocity == Vector2.zero)
		{
			Destroy(body);
		}
	}

	#endregion
}

