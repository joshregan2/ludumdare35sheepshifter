﻿using UnityEngine;
using System.Collections;

public class RDAutoDestroyParticleSystem : MonoBehaviour
{
	#region Fields and Properties

	private ParticleSystem pSystem;

	public delegate void CompletionDelegate();

	public CompletionDelegate Completion {get; set;}

	#endregion

	#region Lifecycle

	// Use this for initialization
	void Start ()
	{
		pSystem = this.gameObject.GetComponent<ParticleSystem>();
	}

	// Update is called once per frame
	void Update ()
	{
		if (pSystem != null)
		{
			if (!pSystem.IsAlive())
			{
				if (Completion != null)
				{
					Completion.Invoke();
				}
				Destroy(this.gameObject);
			}
		}
	}

	#endregion
}