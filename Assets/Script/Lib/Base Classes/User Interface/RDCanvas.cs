using UnityEngine;
using System.Collections;
using System;

public class RDCanvas : RDNavigationStack
{
	#region Fields and Properties
	
	public string CanvasName {get; set;}
	public float ReferenceWidth {get; set;}
	public float ReferenceHeight {get; set;}
	
	private Canvas userInterfaceCanvas;
	public Canvas UserInterfaceCanvas
	{
		get
		{
			if (userInterfaceCanvas == null)
			{
				userInterfaceCanvas = GameObject.Find(CanvasName).GetComponent<Canvas>();
			}
			return userInterfaceCanvas;
		}
		set
		{
			userInterfaceCanvas = value;
		}
	}

	#endregion
	
	#region Lifecycle

	public RDCanvas(string canvasName, float referenceWidth, float referenceHeight)
	{
		CanvasName = canvasName;
		ReferenceWidth = referenceWidth;
		ReferenceHeight = referenceHeight;
	}
	
	#endregion
	
	#region Configuration

	public virtual void PushViewToCanvas(RDView screen)
	{
		screen.RectTransform.SetParent(UserInterfaceCanvas.transform);
		
		CenterAndSetSizeForRectTransform(screen.RectTransform, ReferenceWidth, ReferenceHeight);
		
		PushScreen(screen);
	}
	
	public virtual RDView CreateNewScreen(Type screenType, string screenName)
	{
		GameObject screenGameObject = new GameObject(screenName);
		RDView screen = (RDView)screenGameObject.AddComponent(screenType);
		
		return screen;
	}
	
	#endregion
	
	#region Layout
	
	public void SetSizeForRectTransform(RectTransform rectTransform, float width, float height)
	{
		rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
		rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
	}
	
	public void CenterAndSetSizeForRectTransform(RectTransform rectTransform, float width, float height)
	{
		rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
		rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
		rectTransform.pivot = new Vector2(0.5f, 0.5f);
		SetSizeForRectTransform(rectTransform, width, height);
		rectTransform.localPosition = Vector3.zero;
		rectTransform.localScale = new Vector3(1,1,1);
	}
	
	#endregion
}

