using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RDNavigationStack
{
	#region Fields and Properties
	
	private Stack<RDView> navigationStack;
	
	#endregion
	
	#region Lifecycle
	
	public RDNavigationStack()
	{
		navigationStack = new Stack<RDView>();
	}
	
	#endregion
	
	#region Screen Widget Navigation
	
	public void HideScreen(RDView screen)
	{
		screen.gameObject.SetActive(false);
	}
	
	public void ShowScreen(RDView screen)
	{
		screen.gameObject.SetActive(true);
	}
	
	public void PushScreen(RDView screen)
	{
		if (navigationStack.Count > 0)
		{
			HideScreen(navigationStack.Peek());
		}
		navigationStack.Push(screen);
		ShowScreen(screen);
	}
	
	public void PopScreen()
	{
		if (navigationStack.Count > 1)
		{
			HideScreen(navigationStack.Pop());
			
			ShowScreen(navigationStack.Peek());
		}
	}
	
	public void PopToScreen(RDView screen)
	{
		if (navigationStack.Count > 1 && navigationStack.Contains(screen) == true)
		{
			while(navigationStack.Peek() != screen)
			{
				HideScreen(navigationStack.Pop());
			}
			
			ShowScreen(navigationStack.Peek());
		}
	}
	
	public void ClearNavigationStack()
	{
		navigationStack.Clear();
	}
	
	public RDView GetCurrentScreen()
	{
		return navigationStack.Peek();
	}
	
	#endregion
}

