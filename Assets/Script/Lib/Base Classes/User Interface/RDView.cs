using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class RDView : MonoBehaviour
{
	#region Fields and Properties

	public RectTransform RectTransform {get; set;}

	protected bool userInteractionEnabled;
	protected EventTrigger eventTrigger;
	
	#endregion
	
	#region Lifecycle
	
	protected virtual void Awake() 
	{
		userInteractionEnabled = false;

		RectTransform = gameObject.GetComponent<RectTransform>();
		if (RectTransform == null)
		{
			RectTransform = gameObject.AddComponent<RectTransform>();
		}
	}
	
	// Use this for initialization
	protected virtual void Start() 
	{
		
	}
	
	// Update is called once per frame
	protected virtual void Update() 
	{
		
	}
	
	#endregion

	#region Interaction

	public bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
	{
		return userInteractionEnabled;
	}

	public void SetUserInteractionEnabled(bool enabled)
	{
		userInteractionEnabled = enabled;

		eventTrigger = gameObject.GetComponent<EventTrigger>();
		if (eventTrigger == null)
		{
			eventTrigger = gameObject.AddComponent<EventTrigger>();
			eventTrigger.triggers = new System.Collections.Generic.List<EventTrigger.Entry>();
		}
	}

	public void AddEventTriggerDelegate(EventTriggerType triggerType, UnityEngine.Events.UnityAction<BaseEventData> delegateAction)
	{
		EventTrigger.Entry pointerEventEntry = new EventTrigger.Entry();
		pointerEventEntry.eventID = triggerType;
		pointerEventEntry.callback.AddListener(delegateAction);
		eventTrigger.triggers.Add(pointerEventEntry);
	}

	#endregion

	#region Component Utility
	
	public MonoBehaviour AddNewChildOfType(Type type, string name)
	{
		GameObject childGameObject = new GameObject(name);
		MonoBehaviour component = (MonoBehaviour)childGameObject.AddComponent(type);
		
		RectTransform rectTransform = childGameObject.GetComponent<RectTransform>();
		rectTransform.SetParent(RectTransform);
		
		return component;
	}
	
	public Image AddNewChildImage(string name, string imageSource)
	{
		Image image = (Image)AddNewChildOfType(typeof(Image), name);
		image.sprite = Resources.Load<Sprite>(imageSource);
		
		return image;
	}

	public Text AddNewChildText(string name, string text, string font, int fontSize, Color textColour, TextAnchor textAlignment)
	{
		Text txt = (Text)AddNewChildOfType(typeof(Text), name);
		txt.text = text;
		txt.font = Resources.Load<Font>(font);
		txt.fontSize = fontSize;
		txt.color = textColour;
		txt.alignment = textAlignment;

		return txt;
	}
	
	#endregion

	#region Layout

	public void SetFrameForRectTransform(RectTransform rectTransform, float x, float y, float width, float height)
	{
		rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
		rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
		rectTransform.pivot = new Vector2(0.5f, 0.5f);

		rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, x, 0);
		rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, y, 0);
		rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
		rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);

		rectTransform.localScale = new Vector3(1,1,1);
	}

	public void SetSizeForRectTransform(RectTransform rectTransform, float width, float height)
	{
		rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
		rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
	}
	
	public void CenterAndSetSizeForRectTransform(RectTransform rectTransform, float width, float height)
	{
		rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
		rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
		rectTransform.pivot = new Vector2(0.5f, 0.5f);
		SetSizeForRectTransform(rectTransform, width, height);
		rectTransform.localPosition = Vector3.zero;
		rectTransform.localScale = new Vector3(1,1,1);
	}
	
	#endregion
}

