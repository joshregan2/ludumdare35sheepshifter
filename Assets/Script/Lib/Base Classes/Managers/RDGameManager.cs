using UnityEngine;
using System.Collections;

public class RDGameManager : MonoBehaviour
{
	#region Singleton
	
	public static RDGameManager SharedGameManager {get; private set;}
	
	#endregion
	
	#region Manager Components
	
	public RDContentManager ContentManager {get; set;}
	public RDDataManager DataManager {get; set;}
	public RDSceneManager SceneManager {get; set;}
	
	#endregion
	
	#region Lifecycle
	
	protected virtual void Awake()
	{
		// Configure singleton
		if (RDGameManager.SharedGameManager == null)
		{
			RDGameManager.SharedGameManager = this;
		}

		DontDestroyOnLoad(gameObject);

		Application.targetFrameRate = 60;
	}
	
	// Use this for initialization
	protected virtual void Start()
	{
		ContentManager = gameObject.AddComponent<RDContentManager>();
		DataManager = gameObject.AddComponent<RDDataManager>();
		SceneManager = gameObject.AddComponent<RDSceneManager>();
	}
	
	// Update is called once per frame
	protected virtual void Update()
	{
		
	}
	
	#endregion
}

