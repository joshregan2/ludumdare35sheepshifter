using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RDContentManager : MonoBehaviour
{
	#region Fields and Properties
	
	public const bool DISPLAY_LOGS = false;
	
	public Dictionary<string, string> LoadedAssets {get; private set;}
	
	#endregion
	
	#region Lifecycle
	
	protected virtual void Awake()
	{
		LoadedAssets = new Dictionary<string, string>();
	}
	
	// Use this for initialization
	protected virtual void Start()
	{
		
	}
	
	// Update is called once per frame
	protected virtual void Update()
	{
		
	}
	
	#endregion
	
	#region Loading
	
	public void LoadAsset(string contentFilePath)
	{
		string[] pathComponents = contentFilePath.Split('/');
		string key = pathComponents[pathComponents.Length-1];
		
		LoadedAssets.Add(key, contentFilePath);
		
		//		if (DISPLAY_LOGS == true)
		//		{
		//			//Debug.Log("Loaded asset with key: "+key);
		//		}
	}
	
	public void LoadAssetsFromManifest(List<string> manifest)
	{
		for (int i = 0; i < manifest.Count; i++)
		{
			LoadAsset(manifest[i]);
		}
	}
	
	#endregion
	
	#region Assets
	
	public T GetAsset<T>(string key)
	{
		if (LoadedAssets.ContainsKey(key))
		{
			object asset = Resources.Load(LoadedAssets[key], typeof(T));
			
			return (T)asset;
		}
		else
		{
			throw new Exception("Asset with key '"+key+"' does not exist");
		}
	}
	
	public GameObject GetCloneOfPrefab(string key)
	{
		return (GameObject)GameObject.Instantiate(GetAsset<GameObject>(key));
	}
	
	#endregion
}

