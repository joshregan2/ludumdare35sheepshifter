using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class RDSceneManager : MonoBehaviour
{
	#region Fields and Properties

	public delegate void AsyncLoadingProgressHandler(float progress);
	public delegate void AsyncLoadingCompletionsHandler();
	
	public RDScene CurrentScene {get; private set;}
	
	#endregion
	
	#region Lifecycle
	
	protected virtual void Awake()
	{
		
	}
	
	// Use this for initialization
	protected virtual void Start()
	{
		
	}
	
	// Update is called once per frame
	protected virtual void Update()
	{
		
	}
	
	#endregion
	
	#region Scene Management
	
	public void CloseCurrentScene()
	{
		if (CurrentScene != null)
		{
			// Allow scene to shutdown
			CurrentScene.Shutdown();
			
			// Remove current scene
			Destroy(CurrentScene.gameObject);
		}
	}
	
	public void LoadScene(Type sceneClass)
	{
		CloseCurrentScene();
		
		CurrentScene = (RDScene)RDNode.CreateNewGameObjectOfType(sceneClass, "Scene");
	}
	
	public void LoadScene(string sceneFileName, Type sceneClass)
	{
		// Allow scene to shutdown
		if (CurrentScene != null)
		{
			CurrentScene.Shutdown();
			CurrentScene = null;
		}
		
		SceneManager.LoadScene(sceneFileName);
		
		StartCoroutine(StartSceneOnNextFrame(sceneClass));
	}
	
	private IEnumerator StartSceneOnNextFrame(Type sceneClass)
	{
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		
		CurrentScene = (RDScene)RDNode.CreateNewGameObjectOfType(sceneClass, "Scene");
	}
	
	public void LoadSceneAsync(string sceneFileName, 
	                           Type sceneClass, 
	                           AsyncLoadingProgressHandler asyncLoadingProgressHandler,
	                           AsyncLoadingCompletionsHandler asyncLoadingCompletionsHandler)
	{
		// Allow scene to shutdown
		if (CurrentScene != null)
		{
			CurrentScene.Shutdown();
			CurrentScene = null;
		}
		
		StartCoroutine(AsyncLoading(sceneFileName, sceneClass, asyncLoadingProgressHandler, asyncLoadingCompletionsHandler));
	}
	
	private IEnumerator AsyncLoading(string sceneFileName, 
	                                 Type sceneClass, 
	                                 AsyncLoadingProgressHandler asyncLoadingProgressHandler,
	                                 AsyncLoadingCompletionsHandler asyncLoadingCompletionsHandler)
	{
		AsyncOperation asyncOp = SceneManager.LoadSceneAsync(sceneFileName);
		
		while (!asyncOp.isDone)
		{
			if (asyncLoadingProgressHandler != null)
			{
				asyncLoadingProgressHandler(asyncOp.progress);
			}
			
			yield return null;
		}
		
		//		yield return asyncOp;
		
		CurrentScene = (RDScene)RDNode.CreateNewGameObjectOfType(sceneClass, "Scene");
		
		if (asyncLoadingCompletionsHandler != null)
		{
			asyncLoadingCompletionsHandler();
		}
	}
	
	#endregion
}

