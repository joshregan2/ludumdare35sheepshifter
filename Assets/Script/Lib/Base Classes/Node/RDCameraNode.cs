using UnityEngine;
using System.Collections;

public class RDCameraNode : RDNode
{
	#region Fields and Properties

	public Camera Camera {get; set;}

	#endregion
	
	#region Lifecycle
	
	protected override void Awake() 
	{
		base.Awake();
		Camera = gameObject.GetComponent<Camera>();
		if (Camera == null)
		{
			Camera = gameObject.AddComponent<Camera>();
			Camera.tag = "MainCamera";
		}
	}
	
	// Use this for initialization
	protected override void Start() 
	{
		base.Start();
	}
	
	// Update is override once per frame
	protected override void Update() 
	{
		base.Update();
	}

	public void SetDefaultsFor2D()
	{
		Camera.orthographic = true;
		Camera.orthographicSize = 5;
		Camera.nearClipPlane = 0.3f;
		Camera.farClipPlane = 1000.0f;
	}

	#endregion
}

