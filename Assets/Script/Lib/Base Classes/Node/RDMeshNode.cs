using UnityEngine;
using System.Collections;

public class RDMeshNode : RDNode
{
	#region Fields and Properties
	
	public MeshFilter MeshFilter {get; set;}
	public MeshRenderer MeshRenderer {get; set;}

	#endregion
	
	#region Lifecycle
	
	protected override void Awake() 
	{
		base.Awake();
		
		MeshFilter = gameObject.GetComponent<MeshFilter>();
		if (MeshFilter == null)
		{
			MeshFilter = gameObject.AddComponent<MeshFilter>();
		}
		
		MeshRenderer = gameObject.GetComponent<MeshRenderer>();
		if (MeshRenderer == null)
		{
			MeshRenderer = gameObject.AddComponent<MeshRenderer>();
			MeshRenderer.material = new Material(Shader.Find("Sprites/Default"));
		}
	}
	
	// Use this for initialization
	protected override void Start() 
	{
		base.Start();
	}
	
	// Update is override once per frame
	protected override void Update() 
	{
		base.Update();
	}

	public void SetTexture(string textureFile)
	{
		UpdateTexture(Resources.Load<Texture2D>(textureFile));
	}
	
	public void SetTexture(Texture2D texture)
	{
		UpdateTexture(texture);
	}
	
	protected virtual void UpdateTexture(Texture2D texture)
	{
		MeshRenderer.material.mainTexture = texture;
	}
	
	public void SetSortingLayerAndOrder(string sortingLayerName, int sortingOrder)
	{
		MeshRenderer.sortingLayerName = sortingLayerName;
		MeshRenderer.sortingOrder = sortingOrder;
	}
	
	#endregion

}

