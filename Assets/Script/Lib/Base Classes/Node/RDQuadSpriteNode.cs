using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RDQuadSpriteNode : RDNode
{
	#region Fields and Properties
	
	public MeshFilter MeshFilter {get; set;}
	public MeshRenderer MeshRenderer {get; set;}
	public Mesh SpriteMesh {get; set;}

	public float Width {get; private set;}
	public float Height {get; private set;}

	public float PaddingLeft {get; private set;}
	public float PaddingRight {get; private set;}
	public float PaddingTop {get; private set;}
	public float PaddingBottom {get; private set;}

	#endregion
	
	#region Lifecycle
	
	protected override void Awake() 
	{
		base.Awake();

		MeshFilter = gameObject.GetComponent<MeshFilter>();
		if (MeshFilter == null)
		{
			MeshFilter = gameObject.AddComponent<MeshFilter>();
		}

		MeshRenderer = gameObject.GetComponent<MeshRenderer>();
		if (MeshRenderer == null)
		{
			MeshRenderer = gameObject.AddComponent<MeshRenderer>();
		}
		MeshRenderer.material = new Material(Shader.Find("Sprites/Default"));

		SpriteMesh = new Mesh();
		MeshFilter.mesh = SpriteMesh;

		Width = 0f;
		Height = 0f;

		PaddingLeft = 0f;
		PaddingRight = 0f;
		PaddingTop = 0f;
		PaddingBottom = 0f;

		UpdateMesh();
	}
	
	// Use this for initialization
	protected override void Start() 
	{
		base.Start();
	}
	
	// Update is override once per frame
	protected override void Update() 
	{
		base.Update();
	}

	protected virtual void UpdateMesh()
	{
		float unitWidth = ConvertPixelToUnit(Width);
		float unitHeight = ConvertPixelToUnit(Height);

		float unitPaddingLeft = ConvertPixelToUnit(PaddingLeft);
		float unitPaddingRight = ConvertPixelToUnit(PaddingRight);
		float unitPaddingTop = ConvertPixelToUnit(PaddingTop);
		float unitPaddingBottom = ConvertPixelToUnit(PaddingBottom);

		Vector3[] vertices = new Vector3[]
		{
			new Vector3((-unitWidth/2) + unitPaddingLeft, (unitHeight/2) - unitPaddingTop, 0),
			new Vector3((unitWidth/2) - unitPaddingRight, (-unitHeight/2) + unitPaddingBottom, 0),
			new Vector3((unitWidth/2) - unitPaddingRight, (unitHeight/2) - unitPaddingTop, 0),
			new Vector3((-unitWidth/2) + unitPaddingLeft, (-unitHeight/2) + unitPaddingBottom, 0)
		};

		SpriteMesh.vertices = vertices;

		SpriteMesh.uv = new Vector2[4]
		{
			new Vector2(0,1),
			new Vector2(1,0),
			new Vector2(1,1),
			new Vector2(0,0)
		};
		SpriteMesh.triangles = new int[6]
		{
			0, 1, 2, 1, 0, 3
		};

		SpriteMesh.RecalculateBounds();
	}

	public void SetTexture(Color colour, int width, int height)
	{
		Texture2D solidColourTexture = new Texture2D(width, height);

		Color[] pixels = new Color[width*height];
		for (int i = 0; i < pixels.Length; i++)
		{
			pixels[i] = colour;
		}
		solidColourTexture.SetPixels(pixels);
		solidColourTexture.Apply();

		UpdateTexture(solidColourTexture);
	}

	public void SetTexture(string textureFile)
	{
		UpdateTexture(Resources.Load<Texture2D>(textureFile));
	}

	public void SetTexture(Texture2D texture)
	{
		UpdateTexture(texture);
	}

	protected virtual void UpdateTexture(Texture2D texture)
	{
		MeshRenderer.material.mainTexture = texture;

		if (Width == 0 && Height == 0)
		{
			SetSize2D(new Vector2(texture.width, texture.height));
		}
	}
	
	public void SetSortingLayerAndOrder(string sortingLayerName, int sortingOrder)
	{
		MeshRenderer.sortingLayerName = sortingLayerName;
		MeshRenderer.sortingOrder = sortingOrder;
	}
	
	#endregion
	
	#region Layout

	public virtual void SetPadding(float left, float right, float top, float bottom)
	{
		PaddingLeft = left;
		PaddingRight = right;
		PaddingTop = top;
		PaddingBottom = bottom;

		UpdateMesh();
	}
	
	public virtual void SetFrame2D(float x, float y, float width, float height)
	{
		SetPosition2D(new Vector2(x, y));
		SetSize2D(new Vector2(width, height));
	}
	
	public virtual void SetSize2D(float width, float height)
	{
		SetSize2D(new Vector2(width, height));
	}
	
	public virtual void SetSize2D(Vector2 size)
	{
		Width = size.x;
		Height = size.y;

		UpdateMesh();
	}
	
	#endregion
}

