using UnityEngine;
using System.Collections;

public class RDSpriteNode : RDNode
{
	#region Fields and Properties

	public SpriteRenderer SpriteRenderer {get; set;}

	#endregion
	
	#region Lifecycle
	
	protected override void Awake() 
	{
		base.Awake();
		SpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		if (SpriteRenderer == null)
		{
			SpriteRenderer = gameObject.AddComponent<SpriteRenderer>();
		}
	}
	
	// Use this for initialization
	protected override void Start() 
	{
		base.Start();
	}
	
	// Update is override once per frame
	protected override void Update() 
	{
		base.Update();
	}

	public void Configure(string spriteFile)
	{
		SpriteRenderer.sprite = Resources.Load<Sprite>(spriteFile);
	}

	public void Configure(Sprite sprite)
	{
		SpriteRenderer.sprite = sprite;
	}

	public void SetSortingLayerAndOrder(string sortingLayerName, int sortingOrder)
	{
		SpriteRenderer.sortingLayerName = sortingLayerName;
		SpriteRenderer.sortingOrder = sortingOrder;
	}
	
	#endregion
	
	#region Layout

	public virtual void SetFrame2D(float x, float y, float width, float height)
	{
		SetPosition2D(new Vector2(x, y));
		SetSize2D(new Vector2(width, height));
	}

	public virtual void SetSize2D(float width, float height)
	{
		SetSize2D(new Vector2(width, height));
	}

	public virtual void SetSize2D(Vector2 size)
	{
		if (SpriteRenderer.sprite != null)
		{
			float scaleX = 1.0f;
			float scaleY = 1.0f;

			scaleX = size.x / (float)SpriteRenderer.sprite.texture.width;
			scaleY = size.y / (float)SpriteRenderer.sprite.texture.height;

			gameObject.transform.localScale = new Vector3(scaleX, scaleY, gameObject.transform.localScale.z);
		}
	}

	#endregion
}

