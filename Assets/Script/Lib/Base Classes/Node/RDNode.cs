using UnityEngine;
using System.Collections;
using System;

public class RDNode : MonoBehaviour
{
	#region Fields and Properties
	
	public const float PIXELS_TO_UNIT = 100.0f;
	
	public static bool CamDimensionsSet {get; private set;}
	public static float CamHalfHeight {get; private set;}
	public static float CamHalfWidth {get; private set;}
	
	protected bool userInteractionEnabled;
	protected BoxCollider2D boxCollider2D;
	
	#endregion
	
	#region Lifecycle
	
	protected virtual void Awake() 
	{
		UpdateCamDimensions(false);
		
		userInteractionEnabled = false;
	}
	
	// Use this for initialization
	protected virtual void Start() 
	{
		UpdateCamDimensions(false);
	}
	
	// Update is called once per frame
	protected virtual void Update() 
	{
		
	}

	public void UpdateCamDimensions(bool forceUpdate)
	{
		if (RDNode.CamDimensionsSet == false || forceUpdate == true)
		{
			if (Camera.main != null)
			{
				RDNode.CamDimensionsSet = true;
				CamHalfHeight = Camera.main.orthographicSize;
				CamHalfWidth = Camera.main.aspect * CamHalfHeight;
			}
		}
	}
	
	#endregion

	#region Screen Size
	
	public float GetScreenUnitWidth()
	{
		return (Camera.main.orthographicSize * Screen.width / Screen.height) * 2.0f;
	}
	
	public float GetScreenUnitHeight()
	{
		return Camera.main.orthographicSize * 2.0f;
	}
	
	#endregion
	
	#region Component Utility
	
	public static MonoBehaviour CreateNewGameObjectOfType(Type type, string name)
	{
		GameObject childGameObject = new GameObject(name);
		MonoBehaviour component = (MonoBehaviour)childGameObject.AddComponent(type);
		
		return component;
	}
	
	public virtual MonoBehaviour AddNewChildOfType(Type type, string name)
	{
		GameObject childGameObject = new GameObject(name);
		MonoBehaviour component = (MonoBehaviour)childGameObject.AddComponent(type);
		
		childGameObject.transform.parent = gameObject.transform;
		
		return component;
	}
	
	#endregion
	
	#region Layout
	
	public static float ConvertPixelToUnit(float pixelValue)
	{
		return (pixelValue / PIXELS_TO_UNIT);
	}
	
	public static void SetGameObjectPosition2D(GameObject go, Vector2 position)
	{
		if (go.transform.parent == null)
		{
			go.transform.position = new Vector3(
				-CamHalfWidth + ConvertPixelToUnit(position.x), 
				CamHalfHeight - ConvertPixelToUnit(position.y), 
				go.transform.position.z);
		}
		else 
		{
			go.transform.position = new Vector3(
				ConvertPixelToUnit(position.x), 
				ConvertPixelToUnit(position.y), 
				go.transform.position.z);
		}
	}
	
	public virtual void AddChildNode(RDNode node)
	{
		node.transform.SetParent(gameObject.transform);
	}
	
	public virtual void SetPosition2D(float x, float y)
	{
		SetPosition2D(new Vector2(x, y));
	}
	
	public virtual void SetPosition2D(Vector2 position)
	{
		if (gameObject.transform.parent == null)
		{
			gameObject.transform.position = new Vector3(
				-CamHalfWidth + ConvertPixelToUnit(position.x), 
				CamHalfHeight - ConvertPixelToUnit(position.y), 
				gameObject.transform.position.z);
		}
		else 
		{
			gameObject.transform.position = new Vector3(
				ConvertPixelToUnit(position.x), 
				ConvertPixelToUnit(position.y), 
				gameObject.transform.position.z);
		}
	}
	
	public virtual void SetPosition3D(float x, float y, float z)
	{
		SetPosition3D(new Vector3(x, y, z));
	}
	
	public virtual void SetPosition3D(Vector3 position)
	{
		gameObject.transform.position = position;
	}
	
	public virtual void CenterInParentNode2D()
	{
		if (gameObject.transform.parent != null)
		{
			gameObject.transform.localPosition = Vector3.zero;
		}
	}
	
	#endregion
	
	#region Interaction
	
	public virtual void SetUserInteractionEnabled2D(bool enabled)
	{
		userInteractionEnabled = true;
		
		if (boxCollider2D == null)
		{
			boxCollider2D = gameObject.AddComponent<BoxCollider2D>();
		}
		
		boxCollider2D.enabled = userInteractionEnabled;
	}
	
	public virtual void SetHitBoxSize2D(Vector2 size)
	{
		if (boxCollider2D != null)
		{
			boxCollider2D.size = new Vector2(ConvertPixelToUnit(size.x), ConvertPixelToUnit(size.y));
		}
	}
	
	#endregion
}

