using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RDScene : RDNode
{
	#region Fields and Properties

	public bool InputEnabled {get; set;}

	#endregion
	
	#region Lifecycle
	
	protected override void Awake()
	{
		base.Awake();

		// Initialise scene components
		LoadSceneObjects();
	}
	
	// Use this for initialization
	protected override void Start()
	{
		base.Start();
	}
	
	// Update is called once per frame
	protected override void Update()
	{
		base.Update();

		if (InputEnabled == true)
		{
			HandleInput();
		}
	}
	
	public virtual void Shutdown()
	{
		// Run any shutdown logic, remove components etc.
	}
	
	#endregion

	#region Input

	protected virtual void HandleInput()
	{
		
	}

	#endregion
	
	#region Scene Objects

	protected virtual void LoadSceneObjects()
	{

	}

	public void DestroyAllChildren()
	{
		List<GameObject> children = new List<GameObject>();
		foreach (Transform child in transform)
		{
			children.Add(child.gameObject);
		}
		children.ForEach(child => Destroy(child));
	}

	public GameObject CreatePrefabAsChild(string prefabKey, RDContentManager contentManager)
	{
		// Get copy of desired prefab from Content Manager
		GameObject prefab = contentManager.GetCloneOfPrefab(prefabKey);
		
		// Make child
		prefab.transform.parent = gameObject.transform;
		
		// Return the object reference for use by the caller
		return prefab;
	}

	#endregion

	#region Quad Sprite Convenience

	public RDQuadSpriteNode AddNewChildQuadSpriteNode(string name, string textureFile)
	{
		RDQuadSpriteNode quadSpriteNode = (RDQuadSpriteNode)AddNewChildOfType(typeof(RDQuadSpriteNode), name);
		quadSpriteNode.SetTexture(textureFile);

		return quadSpriteNode;
	}

	public RDQuadSpriteNode AddNewChildQuadSpriteNode(string name, Color colour, int width, int height)
	{
		RDQuadSpriteNode quadSpriteNode = (RDQuadSpriteNode)AddNewChildOfType(typeof(RDQuadSpriteNode), name);
		quadSpriteNode.SetTexture(colour, width, height);
		
		return quadSpriteNode;
	}

	#endregion
}

